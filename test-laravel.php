## TEST LARAVEL

### 1. Un usuario realizó una publicación el día de ayer, hoy un administrador del sistema necesita visualizar la publicación que se hizo y
que usuario la realizó. Explica si el código a continuación es correcto.

~~~
<?php
class Post extends Model {
    /**
    * Model Post.php
    * Get the author of the post.
    */
    public function user()
    {
        return $this->hasOne(User::class);
    }
}
?>
~~~

~~~
<?php
class PostController extends Controller {
    * Method GET 
    * api : api/post/{id}
    * Get the post.
    */
    public function show($id)
    {
        $post = Post::find($id);
        return $post;
    }
}
?>
~~~

Repuesta = La consulta debe incluir users para poder ver el usuario que la realizo 
debe ser asi  $post = Post::with('user')->find($id);


### 2. Un desarrollador necesita dar solución a una petición, la cual debe cargar cierta información. Esta información tiene es un conjunto de registros los cuales tiene los campos: User_Id, Tittle_Post, Comment. Se debe tener en consideración el tiempo de ejecución, que el usuario que realiza el port exista y que el número de registros sea mínimo 10000. Explica si el código a continuación es correcto.

~~~
<?php
class PostController extends Controller {
    /*
    * User has relationship one to many with Post
    * Post has relationship one to many with Comments
    * User has relationship one to many with Comments
    * $request = [];
    *
    */
    public function setData(Request $request){
        $data = $request->data;
        for($element in $data){
            $user = User::find($element['User_Id']);
            if($user){
                $post = Post::create([
                    'title' => $element['Tittle_Post'],
                ])
                DB->connection('data_base')->table('comments')->insert([
                    'comment' => $element['Comment'],
                    'post_id' => $post->id,
                    'user_id' => $user->id
                ]);
            }
        }
    }
}
?>
~~~

Respueta:

primero habria que verificar que la cantidad de registros sean 10000 con un count de data

Podria er algo asi 

//se verifica que sean al menos 10000

if(count($data)>=10000){

    for($element in $data){
            $user = User::find($element['User_Id']);
            if($user){

                //aqui falta insertar el id del usuario en la relacion del post con user
                //asi como lo modifique 
                $post = Post::create([
                    'title' => $element['Tittle_Post'],
                    'user_id' => $user->id
                ])
                DB->connection('data_base')->table('comments')->insert([
                    'comment' => $element['Comment'],
                    'post_id' => $post->id,
                    'user_id' => $user->id
                ]);
            }
        }

}

De resto  esta bien.


### 3. Se necesita descargar un archivo con las alertas del sistema que los usuarios han generado, el número de registros de alertas superan
los 6k registros. Explica el código a continuación y qué necesitas agregar para que este funcione.

~~~
<?php
/*
 * $request->ids = Id's of the Users that i want to see in the report.
 *
*/
public function downloadAlerts(Request $request)
    {
        ImportAlerts::dispatch(
            $request->ids
        );

        $response         = new \stdClass();
        $response->status   = 200;
        $response->message   = 'The report is being generated, you will receive an email when finished.';

        return response()->json(
            $response,
            200
        );
    }

?>
~~~

// Faltaria enviar el correo al finalizar la tarea 
// ImportAlerts::dispatch()->onQueue('emails');
//solo faltaria esa sentencia 
