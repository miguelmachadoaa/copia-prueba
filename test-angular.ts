
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, of, tap, retry } from 'rxjs';


/**
 * -- Primer Problema:
 *
 * Mostrar el contenido de los observables disponibles en el servicio ContentService
 * en el template del componente AppComponent. Adicionalmente:
 *  Title debe estar todo en mayúsculas
 *  Body debe estar todo en minúsculas
 *  Footer debe estar capitalizado
 *
 *
 * -- Segundo Problema:
 *
 * Se necesita que cuando el usuario haga click en el botón "Change Title", asociado al componente ChangeContentComponent,
 * actualize el titulo ("title") que se ve en el componente AppComponent
 *
 *
 * -- Tercer Problema
 *
 * Se necesita que cuando el usuario haga click en el botón "Call Api" asociado al componente ChangeContentComponent
 * haga el llamado a la api manejada por el servicio GetContentService. En caso de que la conexión sea exitosa debe
 * mostrarse el mensaje "Connection to API Success", caso contrario debe mostrarse el mensaje "Connection to API Failed"
 * en el template asociado al componente ChangeContentComponent
 */

@Component({
  selector: 'app-root',
  template: `
    <div class="container">
      <section>
        <div class="card">
          <div class="card-title">{{ title | uppercase}}</div>
          <br />
          <hr />
          <br />

          <div class="card-body">{{ body | lowercae}}</div>
          <br />
          <hr />
          <br />

          <div class="card-footer">{{ footer | titlecase }}</div>
        </div>
      </section>
      <change-content></change-content>
    </div>
  `,
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title: any;
  body: any;
  footer: any;

  constructor() {}
}

@Injectable({
  providedIn: 'root',
})
export class ContentService {
  private title: BehaviorSubject<string> = new BehaviorSubject(
    'Este es el titulo'
  );
  private body: BehaviorSubject<string> = new BehaviorSubject(
    'Este es el contenido principal de la pagina'
  );
  private footer: BehaviorSubject<string> = new BehaviorSubject(
    'Este es el footer '
  );

  private title$ = this.title.asObservable();
  private body$ = this.body.asObservable();
  private footer$ = this.footer.asObservable();

  constructor() {}

  getTitle() {
    return this.title$;
  }

  getBody() {
    return this.body$;
  }

  getFooter() {
    return this.footer$;
  }

  changeTitle() {
    
  }
}

@Component({
  selector: 'change-content',
  template: `
    <section>
      <div class="card">
        <button (click)=changeTitle(this.title)> Change Title </button>
        <button (click)=callApi()> Call Api </button>
        <section> Connection to API Success  </section>
        <section> Connection to API Failed  </section>
      </div>
    </section>
  `,
})
export class ChangeContentComponent {
  newTitle = 'Este es el titulo modificado por otro componente';
  hasError = false;
  hasContent = false;

  constructor( private service: GetContentService) {}

  changeTitle(title) {
    // Desarrollar el cuerpo del método
    title = this.newTitle;
    return title;
  }

  callApi(){
      this.getContent().susbribe(data=>{
        this.title='Connection to API Success';
      },
      err=>{
        this.title='Connection to API Failed';
      }
  }
}




@Injectable({
  providedIn: 'root',
})
export class GetContentService {
  constructor(
    private http: HttpClient
  ) {}

  getContent(){
    const url = 'https://sitenotexist.com/content/0';

    return this.http.get(url).pipe(
      retry(2),
      map( () => 'Respuesta modificada por el servicio'),
      catchError( (error) => of(error)),
      tap((value) => console.log('check for value', value)),
      map( () => ['Respuesta modificada por el servicio nuevamente']),
    );
  }
}
