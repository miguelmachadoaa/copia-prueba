# Prueba Fullstack

## Atención

Es necesario que el candidato comparta la totalidad de su pantalla con el entrevistador, también se requiere que la cámara y el micrófono estén activos mientras se mantiene la sesión en vivo

## 1. Propósito

Lo que se busca para esta prueba es visualizar que el desarrollador tenga conocimientos en Laravel y Angular, que se cuenta con fuertes bases en lógica y que sobre todo este dispuesto a aprender y que siempre quiera generar mayor valor al equipo en donde se encuentra

## 2. Requerimientos

- Fuertes conocimientos en Javascript
- Fuertes conocimientos en Angular
- Conocimientos en el patrón Container / Presentation
- Fuertes conocimientos en el manejo de Git
- Fuertes conocimientos en lenguaje PHP
- Fuertes conocimientos en framework Laravel
- Conocimientos en el patrón Repository

## 3. Prueba Angular

Responder las preguntas disponibles en el archivo test-angular.ts, mientras se tiene la sesión en vivo con el entrevistador. Esta prueba contiene 3 preguntas que requieren de análisis para poder implementar la mejor solución al caso planteado.

Se requiere el desarrollo de código para lograr implementar las soluciones

## 4. Prueba Laravel

Responder las preguntas disponibles en el archivo test-laravel.php, mientras se tiene la sesión en vivo con el entrevistador. Esta prueba contiene 3 preguntas que requieren de análisis para evaluar la solución planteada y en caso de no ser correcta, plantear una nueva

Esta prueba no requiere la modificación o refactorización de su contenido

## 5. Pasos post prueba

Ya resueltas las preguntas de la prueba, hacer un fork de este repositorio y compartirlo con la siguiente cuenta de correo: technology@claroinsurance.com

Sigue los pasos adicionales que el entrevistador indique

En caso de dudas o preguntas referentes al flujo de la entrevista o algún otro punto, por favor comunicarle al entrevistador
